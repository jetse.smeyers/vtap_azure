# vTap_Azure

For documentation, please refer to [the Wiki](https://gitlab.com/jetse.smeyers/vtap_azure/-/wikis/home)  

The repository is seperated into two folders to improve convenience while installing the tools necessary to perform Windows tcp dump over SSH. Download the appropriate directory to your host machine, all the files needed should be included. 
