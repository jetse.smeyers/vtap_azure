#!/bin/bash

#Add the ip addresses of the hosts you want to monitor in the ./monitored_hosts file in the following format:
#[username]@[ipv4] or [ipv4]

#Please provide the NIC name you want to pipe all the traffic to.
NIC=eth1

#Please provide the location of the private key file.
privateKeyFilePath="/home/demo/.ssh/id_rsa"



#reading own ip from routing table
ownip=$(ip route get 1 | awk '{print $7}')
#making sure 
ip link set $NIC up

while IFS= read -r line; do
  remoteIPs+=($line)
done < ${BASH_SOURCE%/*}/monitored_hosts

total="${#remoteIPs[@]}"
count=0
runSSH=":"

echo Connecting to the following hosts:

while [ $count -lt $total ] 
do
   echo ${remoteIPs[$count]}
   runSSH="$runSSH & ssh -i $privateKeyFilePath -o StrictHostKeyChecking=no ${remoteIPs[$count]} \"C:\WinDump.exe -U -w - not (host $ownip and tcp port 22)\" | tcpreplay --intf1=$NIC -"
   count=$((count + 1))
done

eval "$runSSH"
