<#

This script will install OpenSSH.Server feature and enable ssh key authentication.
To execute, simply run the script within a PowerShell command-line shell with Administrator rights.

Arguments (all are optional):
	-user [username]				Enables SSH access for a particular non-Admin user.
	-enablecurrentuser				Enables SSH access for the current user. (only necessary if you're not an Administrator)
	-key [path_to_public_key_file]	Specify the path to the file containing the public key (id_rsa.pub).
	-adminaccess					Enable public key authentication for Administrator accounts.

Example:
	installSSHkey.ps1 -user some_user_name -key C:\some_file_path -admin-access
#>


param(
    [string]$user,

    [System.IO.FileInfo]$key,

    [switch]$adminaccess,

    [switch]$enablecurrentuser);


$username = $user;
$enableForCurrentUser = $enablecurrentuser;
$publicKeyPath = Resolve-Path -path $key;
$key = Get-Content $publicKeyPath;
$adminAccess = $adminaccess;



#Add OpenSSH feature
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0

#Allow incoming connections to OpenSSH server in Windows Firewall
New-NetFirewallRule -Name sshd -DisplayName 'OpenSSH Server (sshd)' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 22

#Start service and configure automatic start
Start-Service sshd
Set-Service -Name sshd -StartupType 'Automatic'


#Append key to authorized_keys file of current user/newly created user (depending on the given arguments)
if (($username -or $enableForCurrentUser) -and $key) {
	if ($username) {
		$path = 'C:\Users\' + $username + '\.ssh\'
	}
	if ($enableForCurrentUser) {
		$path = 'C:\Users\' + [System.Security.Principal.WindowsIdentity]::GetCurrent().Name + '\.ssh\'
	}

	if (!(Test-Path $path))
	{
		New-Item $path -ItemType Directory
	}
	if (!(Test-Path "$path + 'authorized_keys'" -PathType Leaf))
	{
		New-Item -path $path -name 'authorized_keys' -type "file" -value $key
	}
	else
	{
		Add-Content -path $path + 'authorized_keys' -value "'r'n " + $key
	}
	Write-Host 'SSH key copied!'
}

#Configure SSH access for Administrator accounts if needed
if ($adminAccess -and $key) {
	if (!(Test-Path 'C:\ProgramData\ssh\administrators_authorized_keys' -PathType Leaf))
	{
		New-Item -path 'C:\ProgramData\ssh\' -name 'administrators_authorized_keys' -type "file" -value $key
	}
	else
	{
		Add-Content -path 'C:\ProgramData\ssh\administrators_authorized_keys' -value "'r'n " + $key
	}
	
	#Set correct permissions on administrators_autorized_keys file
	$acl = Get-Acl C:\ProgramData\ssh\administrators_authorized_keys
	$acl.SetAccessRuleProtection($true, $false)
	$administratorsRule = New-Object system.security.accesscontrol.filesystemaccessrule("Administrators","FullControl","Allow")
	$systemRule = New-Object system.security.accesscontrol.filesystemaccessrule("SYSTEM","FullControl","Allow")
	$acl.SetAccessRule($administratorsRule)
	$acl.SetAccessRule($systemRule)
	$acl | Set-Acl

	Write-Host 'SSH access for Administrator accounts successfully enabled'
}

$scriptDir = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
if(Copy-Item -Path "$scriptDir\WinDump.exe" -Destination "C:\") 
{
    Write-Host 'WinDump.exe copied to C:\WinDump.exe'
}

Write-Host '--> Script completed!'
exit
